from setuptools import setup

setup(
    name='invest_tools',
    version='0.1.0',
    packages=['datamonk_investtools'],
    url='',
    license='',
    author='Vit Mrnavek',
    author_email='vit.mrnavek@datamonk.cz',
    description='package for my personal investment activities'
)

